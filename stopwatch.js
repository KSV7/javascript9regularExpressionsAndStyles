window.onload = () => {
    const get = id => document.getElementById(id);

    var h1 = document.getElementsByTagName('h1')[0];
    var start = get('startStopwatch');
    var stop = get('stopStopwatch');
    var reset = get('resetStopwatch');

    var t;
    var sec = 0;
    var min = 0;
    var hours = 0;

    function timeOn(){
        sec++;
        if (sec > 59) {
            sec = 0;
            min++;
            if (min > 59) {
                min = 0;
                hours++;
            }
        }
    }

    function add() {
        timeOn();
        h1.textContent = (hours > 9 ? hours : "0" + hours) 
                + ":" + (min > 9 ? min : "0" + min)
                + ":" + (sec > 9 ? sec : "0" + sec);
        timer();
    }

    function timer() {
        t = setTimeout(add, 1000);
    }

    start.onclick = () => {
        timer();
    }

    stop.onclick = function() {
        clearTimeout(t);
    }

    reset.onclick = function() {
        h1.textContent = "00:00:00";
        sec = 0;
        min = 0;
        hours = 0;
        clearTimeout(t);
    }
}